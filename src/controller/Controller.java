package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoubleLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations(String archivo) {
		manager.loadStations(archivo);
	}
	
	public static void loadTrips(String archivo) {
		manager.loadTrips(archivo);
	}
		
	public static IDoubleLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static IDoubleLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
