package api;

import model.data_structures.IDoubleLinkedList;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager<T> {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	void loadStations(String stationsFile);
	

	public IDoubleLinkedList <VOTrip> getTripsOfGender (String gender);
	
	
	public IDoubleLinkedList <VOTrip> getTripsToStation (int stationID);


	
}
