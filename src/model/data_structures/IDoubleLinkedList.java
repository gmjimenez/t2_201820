package model.data_structures;

import java.util.Comparator;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoubleLinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add(T object);
	
	public void addAtEnd(T object);
	
	public T getElement(T pElement);
	
	public T getCurrentElement(int i);
	
	
	
	void deleteAtK(int K) throws Exception;

	void delete(T x) throws Exception;
	
	

}
