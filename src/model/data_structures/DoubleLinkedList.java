package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>> implements IDoubleLinkedList<T> {

	
	private Nodo primerNodo;
	private int size;
	
	
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		
		
		return null;
	}
	
	public DoubleLinkedList() {
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public Integer getSize() {
		// DONE Auto-generated method stub
		
		size = 0; 
		Nodo actual = primerNodo; 
		while(actual !=null) {
			size++;
			actual = actual.darSgte();
		}
		
		return size;
	}

	@Override
	public void add(T object) {
		// DONE Auto-generated method stub
		if(primerNodo == null) {
			primerNodo = new Nodo<T>(object);
			size++; 
		
		}
		
		else {
			
			addAtEnd(object);

		}
		
	}

	@Override
	public void addAtEnd(T nuevo) {
		// DONE Auto-generated method stub
		Nodo<T> x = new Nodo<T>(nuevo);
		if(primerNodo == null) {
			x.cambiarSiguiente(primerNodo);
			primerNodo = x;
			size++; 
		}
		else {
			Nodo<T> ultimo = buscarUltimo(nuevo);
			ultimo.insertarDespues(new Nodo<T>(nuevo));
			size++;
		}
		
	}

	@Override
	public T getElement(T pElement) {
		// DONE Auto-generated method stub
			Nodo<T> actual = primerNodo;
			while(actual != null) {
				if(actual.darItem().equals(pElement)) {
					return actual.darItem(); 
				}
				actual = actual.darSgte();
			}
		return null;
	}

	@Override
	public T getCurrentElement(int indice) {
		// DONE Auto-generated method stub
		int comparador = 0; 
		if(primerNodo == null)
			return null; 
		else {
			Nodo actual = primerNodo; 
			while(actual !=null) 
			{
				if(indice == comparador) {
					return (T) actual.darItem();
				}
				comparador++; 
				actual = actual.darSgte(); 
			}

		}
		return null; 
	}

	@Override
	public void delete(T x) throws Exception {
		// DONE Auto-generated method stub
		if(primerNodo == null) {
			throw new Exception();
		}
		else  if(primerNodo.darItem().equals(x)) {
			primerNodo = primerNodo.darSgte();
		}
		else {
				Nodo anterior = buscarAnterior(x);
				if(anterior == null) {
					throw new Exception();
				}
				
				//DONE Eliminar el nodo/elemento 
				anterior.desconectarSiguiente();
		}
		size--; 
	}

	@Override
	public void deleteAtK(int k) throws Exception {
		// DONE Auto-generated method stub
		Nodo<T> actual = primerNodo; 
		int indice = 0; 
		if(actual == null) {
			throw new Exception();
		}
		while(actual!= null && indice != k) {
			actual = actual.darSgte(); 
			indice++; 
		}
		if(indice==k) {
			actual.eliminarActual();
			size--;

		}
	}


	//M�todos auxiliares
	
	public Nodo buscarAnterior(T element) {
		Nodo<T> anterior = null; 
		Nodo<T> actual = primerNodo;
		while(actual!=null && !(actual.darItem().equals(element))) {
			anterior = actual; 
			actual = actual.darSgte();
		}
		return actual == null? null: anterior;
	}
	
	public Nodo buscarUltimo(T element) {
		if(primerNodo == null) {
			return null; 
		}
		else {
			Nodo actual = primerNodo;
			while(actual.darSgte() != null) {
				actual = actual.darSgte();
			}
			return actual; 
		}
		
	}

}
