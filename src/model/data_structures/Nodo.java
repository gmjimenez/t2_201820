package model.data_structures;

public class Nodo<T extends Comparable<T>> {

	//Atributos
	private Nodo siguiente; 
	
	private Nodo anterior; 
	
	private T item; 



	
	//M�todos
	
	public Nodo(T element) {
		item = element; 
	}
	
	public T darItem() {
		return item; 
	}
	public Nodo<T> darSgte() {
		return siguiente;

	}

	public void cambiarSiguiente( Nodo<T> nSiguiente )
	{
		siguiente = nSiguiente;


	}

	public void desconectarSiguiente( )
	{
		siguiente = siguiente.siguiente;
	}

	
	public void insertarDespues( Nodo pNode )
	{
		pNode.siguiente = siguiente;
		pNode.anterior =this;
		if(siguiente !=null) {
			siguiente.anterior = pNode;
		}
		siguiente = pNode;
	}
	



	public Nodo darAnt() {
		return anterior;

	}

	public void cambiarAnt( Nodo ante )
	{
		anterior = ante;


	}

	public void desconectarAnterior( )
	{
		anterior = anterior.anterior;
	}

	public void insertarAntes( Nodo pNode )
	{
		pNode.siguiente = this;
		pNode.anterior = anterior;
		if(anterior !=null)
			anterior.siguiente = pNode;
		anterior = pNode;
	}
	
	public void eliminarActual() {
		if(anterior!=null)
			anterior.siguiente = siguiente;
		
		if(siguiente!=null)
			siguiente.anterior = anterior;
	}


}
