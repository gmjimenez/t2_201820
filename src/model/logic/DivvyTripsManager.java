package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {


	private IDoubleLinkedList<VOTrip> stations;
	private IDoubleLinkedList<VOTrip> viajesSeparados; 
	
	
	
	
	public void loadStations (String stationsFile) {
		// DONE Auto-generated method stub
		File arch = new File(stationsFile);

		try {
			FileReader reader = new FileReader(arch);
			BufferedReader in = new BufferedReader(reader);
			String linea = in.readLine();
			linea = in.readLine();

			while(linea!=null) {

				String[] viajesTotal = linea.split(",");
				stations = new DoubleLinkedList<>();
				
				int id = Integer.parseInt(viajesTotal[0]);
				String name = viajesTotal[1];
				String city = viajesTotal[2];
				double latitude = Double.parseDouble(viajesTotal[3]);
				double longitude = Double.parseDouble(viajesTotal[4]);
				int capacity =Integer.parseInt(viajesTotal[5]);
				String onlineDate = viajesTotal[6];
				
				VOTrip stationP = new VOTrip(id, name, city, latitude, longitude, capacity, onlineDate);
				stations.add(stationP);

				
				linea = in.readLine();
			}

			in.close();
			reader.close();


		} catch ( IOException k) {
			// DONE Auto-generated catch block


		}
	}


	public void loadTrips (String trips) {
		// DONE Auto-generated method stub
		//Leer el archivo de texto
		File arch = new File(trips);

		try {
			FileReader reader = new FileReader(arch);
			BufferedReader in = new BufferedReader(reader);
			String linea = in.readLine();
			linea = in.readLine();

			while(linea!=null) {

				String[] viajes = linea.split(",");
				if(viajes.length == 12) {
					int pId = Integer.parseInt(viajes[0]);
					String pStartTime = viajes[1];
					String pEndTime = viajes[2];
					int pBikeId = Integer.parseInt(viajes[3]);
					int pDuration = Integer.parseInt(viajes[4]);
					int pFromSId = Integer.parseInt(viajes[5]);
					String pFromSName = viajes[6];
					int pToSId = Integer.parseInt(viajes[7]);
					String toSName = viajes[8];
					String pUsertype = viajes[9];
					String pGender = viajes[10];
					int pBirth = Integer.parseInt(viajes[11]);
					VOTrip trip= new VOTrip(pId, pStartTime, pEndTime, pBikeId, pDuration, pFromSId, pFromSName, pToSId, toSName, pUsertype, pGender, pBirth);
					//A�adir a la lista
					viajesSeparados = new DoubleLinkedList<>();
					viajesSeparados.add(trip);
					
				}

				linea = in.readLine();
			}
			in.close();
			reader.close();


		} catch ( IOException k) {
			// DONE Auto-generated catch block


		}





	}

	@Override
	public IDoubleLinkedList <VOTrip> getTripsOfGender (String gender) {
		// DONE Auto-generated method stub

		IDoubleLinkedList<VOTrip> tripsOfGender = new DoubleLinkedList<>();

			for (int i = 0; i < viajesSeparados.getSize(); i++) {
			if(viajesSeparados.getCurrentElement(i) != null) {
				if(viajesSeparados.getCurrentElement(i).getGender().equalsIgnoreCase(gender)) {
					tripsOfGender.add(viajesSeparados.getCurrentElement(i));
				}
			}
		}

		return tripsOfGender; 



	}

	@Override
	public IDoubleLinkedList <VOTrip> getTripsToStation (int stationID) {
		// DONE Auto-generated method stub

		IDoubleLinkedList<VOTrip> staticT = new DoubleLinkedList<>();

		for (int i = 0; i < viajesSeparados.getSize(); i++) {
			if(viajesSeparados.getCurrentElement(i) !=null) {
				if(viajesSeparados.getCurrentElement(i).getToStationId()== stationID){
					staticT.add(viajesSeparados.getCurrentElement(i));
				}

			}
		}

		return staticT; 
	}






	}
