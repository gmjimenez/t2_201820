import static org.junit.jupiter.api.Assertions.*;

import java.util.Comparator;

import org.junit.jupiter.api.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.Nodo;
import model.vo.VOTrip;

public class DoubleLinkedListTest {

	

	@Test
	<T> void testAdd( ) {
		try {
			
			IDoubleLinkedList n =  new DoubleLinkedList();
			T i = (T) "Male";
			T x = (T) "";
			
			
			
			n.add( x);
			n.delete(i);
			fail("Se espera Exception");
			
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		
		
	}

	@Test
	<T> void testDelete() {
		try {

			IDoubleLinkedList n =  new DoubleLinkedList();
			VOTrip i = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
			n.delete(i);
			fail("Se espera Exception");

		} catch (Exception e) {
			// TODO: handle exception

		}
	}
	
	@Test
	<T> void testGetCurrentElement() {
		
		try {
			IDoubleLinkedList n =  new DoubleLinkedList();
			T element = (T) n.getCurrentElement(0); 
			assertNull(element);
		}
		
		catch(Exception e)
		{
			fail("La variable debe ser null");
		}
		
	}
	
	@Test
	<T> void testAddEnd() {
		try {
			IDoubleLinkedList n =  new DoubleLinkedList();
			VOTrip i = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
			n.addAtEnd(i);
			T element = (T) n.getCurrentElement(0);
			assertNotNull(element);

		}
		catch (Exception e) {
			
		}
	}
	
	@Test
	void testSize() {
		try {
		IDoubleLinkedList n =  new DoubleLinkedList();
		VOTrip i = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
		n.add(i);
		int size =n.getSize();
		assertTrue(size == 1);
	}
		catch(Exception e) {
			fail("El tama�o debe ser 1");
		}
	}
	
	@Test
	<T> void testGetElement() {
		try {
			IDoubleLinkedList n =  new DoubleLinkedList();
			VOTrip i = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
			T k = (T) n.getElement(i);
			assertNull(k);
			
		}
		catch (Exception e) {

		}


	}
	
	@Test
	void testDeleteAtK() {
		
		
		try {
			IDoubleLinkedList n =  new DoubleLinkedList();
			n.deleteAtK(0);
			fail("Deber�a lanzar excepci�n");
			
		}
		catch (Exception e) {
			
		}
	}

}
